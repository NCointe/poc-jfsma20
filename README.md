# POC-JFSMA20

Preuve de concept associée à l'article présenté aux Journées Francophones sur les Systèmes Multi-Agents (JFSMA) 2020

## Mofifications depuis la soumission de l'article sur Easychair

Les images présentées en Figure 4 doivent être remplacées par ![estimated_goals_probability.pdf](estimated_goals_probability.pdf) et ![average_estimated_goal1_probability.pdf](average_estimated_goal1_probability.pdf)

## Utilisation

Le dépot contient déjà les résultats d'un run. Vous pouvez cependant modifier et/ou lancer l'expérience sur votre propre machine pour 

### Exécution

**Attention**, les scripts sont fait pour des systèmes POSIX, testés sous Archlinux (Rolling update Février 2020) et Ubuntu 18.04LTS sur un processeur i7-7500U @ 3.50GHz, avec 16Go RAM et un JRE Java(TM) SE Runtime Environment (build 13+33) 64-Bit Server VM (build 13+33, mixed mode, sharing) ainsi que OpenJDK 64-Bit Server VM (build 11.0.6+10-post-Ubuntu-1ubuntu118.04.1, mixed mode, sharing). Il devrait en théorie être possible d'utiliser ces outils sous Mac OSX, mais cela n'a pas été testé. Les résultats graphiques sont produits avec GNUplot (version utilisée : 5.2 patchlevel 8).

Pour lancer l'expérience, utilisez le script fourni avec `bash LinuxLauncher.sh --loop 10`. Cela correspond à une répétition de 10 runs de la même expérience, suivi de l'agrégation des résultats et la production d'un rapport.

Comptez environ un quart d'heure de temps d'exécution selon le matériel.

### Résultats

Les résultats vous sont présentés dans le fichier généré report.html

## Versions

>-**V1.0** *Dimanche 23 Février 2020* - Commit initial, version de la soumission.
>
>-**V1.1** *Mercredi 26 Février 2020* - Modification du processus de confiance. Les résultats s'en trouvent légèrement modifiés : l'image des agents utilisant une IRS de dissimulation voient leur image évoluer lentement dans un premier temps puis se stabiliser.
