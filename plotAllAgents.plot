set encoding utf8
set terminal pdf size 10cm, 10cm
set output "estimated_goals_probability.pdf"

#set title "Evolution of the goals probability"
set xlabel "P(g1)"
set ylabel "P(g2)"
set zlabel "P(g3)"

set ticslevel 0
set key off # no legend

set lmargin 0

set xrange [0:1]
set yrange [0:1]
set zrange [0:1]

set view 60, 170, 1, 1.1 # Position the camera

splot 'out/1/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#0099FF",\
	'out/1/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF0000",\
	'out/1/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF00",\
	'out/1/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#00FF99",\
'out/2/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/2/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/2/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/2/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/3/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/3/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/3/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/3/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/4/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/4/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/4/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/4/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/5/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/5/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/5/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/5/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/6/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/6/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/6/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/6/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/7/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/7/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/7/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/7/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/8/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#0099EE",\
	'out/8/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#EE0088",\
	'out/8/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99EE00",\
	'out/8/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#00EE99",\
'out/9/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#00AAFF",\
	'out/9/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8800",\
	'out/9/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#99FF11",\
	'out/9/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#11FF99",\
'out/10/ir1.csv' using 1:2:3 with lines title 'ir1' lc rgb "#0044FF",\
	'out/10/ir2.csv' using 1:2:3 with lines title 'ir2' lc rgb "#FF8888",\
	'out/10/ir3.csv' using 1:2:3 with lines title 'ir3' lc rgb "#44FF00",\
	'out/10/ir4.csv' using 1:2:3 with lines title 'ir4' lc rgb "#00FF44"
