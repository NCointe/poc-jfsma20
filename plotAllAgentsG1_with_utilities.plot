set encoding utf8
set terminal pdf size 10cm, 10cm
set output "estimated_goal1_probability.pdf"

#set title "Evolution of the goal 1 probability"
set xlabel ""
set ylabel "P(g1)"

set ticslevel 0

set xrange [0:30]
set yrange [0.15:1]

set format x "" # No x labels 

set size 1, 0.7
set origin 0, 0.3
set bmargin 0

set key top left
set multiplot

plot 'ir1.csv' using 1 with lines title 'ir1' lc rgb "#0099FF",\
	'ir2.csv' using 1 with lines title 'ir2' lc rgb "#FF0000",\
	'ir3.csv' using 1 with lines title 'ir3' lc rgb "#99FF00",\
	'ir4.csv' using 1 with lines title 'ir4' lc rgb "#00FF99"

unset title
set bmargin
set format x
set size 1.0, 0.3
set origin 0.0, 0.0
set tmargin 0
set autoscale y
#set format y "%1.0f"
#set ytics 25
#set ylabel "volume" offset 1
set ylabel "utilities"
plot 'utilities.csv' using 1 with lines title 'EUt' lc rgb "#00CC00",\
     'utilities.csv' using 2 with lines title 'EUo' lc rgb "#CC0000"
unset multiplot
