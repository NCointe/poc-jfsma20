#!/bin/bash

NUM_OF_LOOPS=1
VERBOSE=false
SIMULATION_SPEED=1
PROGRESS_BAR_LENGTH=20
JAVA_HOME=$(which java)
ANT_HOME=$(which ant)

re='^[0-9]+$' # regular expression to verify if a variable is a number

# This function display an ascii progress bar
displayState()
{
  tput civis      -- invisible # Hide cursor
  echo -en " $1\t/$2\t$SECONDS s\t ["
  
  RATIO=$(echo "$1/$2" | bc -l)           # Evaluate the progress
  
  TIME_ESTIMATION=$(echo "$SECONDS/$RATIO" | bc -l )
  E_TIME_ESTIMATION=${TIME_ESTIMATION%.*} # Convert float to int

  NOW_H=$(date "+%-H")
  NOW_M=$(date "+%-M")
  NOW_S=$(date "+%-S")
  REMAIN_TIME=$(($E_TIME_ESTIMATION-$SECONDS)) 
 

  S_ESTIMATION=$(( ($REMAIN_TIME + $NOW_S) % 60 ))
  PROPAGATE=$(( ($REMAIN_TIME + $NOW_S) - $S_ESTIMATION ))
  M_ESTIMATION=$(( ($NOW_M + $PROPAGATE/60 ) %60 ))
  PROPAGATE=$(( ($NOW_M + $PROPAGATE/60 ) - $M_ESTIMATION))
  H_ESTIMATION=$(( ($NOW_H + $PROPAGATE/60 ) %24 ))

  for I in `seq 0 $PROGRESS_BAR_LENGTH`; do 

    CURSOR=$(echo "$I/$PROGRESS_BAR_LENGTH" | bc -l)
    LOWER=$(echo "$CURSOR<$RATIO" | bc -l) 
  
    if [ $LOWER -eq "1" ]; then
      echo -n "#"
    else
      echo -n "-"
    fi
  done

  echo -en "] Estimated end : $H_ESTIMATION h $M_ESTIMATION m $S_ESTIMATION s \r"
  tput cnorm   -- normal       # Restore the cursor visibility
}

usage()
{
  echo -e "Usage : $0 [options]\n\n\t Available options are :\n\t\t--loop N\tIndicates the number N of experiments to run\n\t\t-v or --verbose\tto activate verbose mode\n\t\t"
}

if [[ $# != 0 ]]
  then
    while [[ $# -gt 0 ]]
      do
        case "$1" in
          --loop) 
            shift
            if ! [[ $1 =~ $re ]] ; then
              echo "error: argument is not a number" >&2; 
              exit 1
            fi
            NUM_OF_LOOPS=$1
            ;;
          -l) 
            shift
            if ! [[ $1 =~ $re ]] ; then
              echo "error: argument is not a number" >&2; 
              exit 1
            fi
            NUM_OF_LOOPS=$1
            ;;
					-v)
            VERBOSE=true
            ;;
          --verbose)
            VERBOSE=true
            ;;
          --*) usage
              exit 1
            ;;
          *) usage
              exit 1
            ;;
        esac
       shift
      done
  else
    usage
    exit 1
fi

if [[ $VERBOSE == true ]]
  then
    echo "Verbose mode is on"
fi

# Prepare the results folder
rm -r ./out/* > /dev/null

#firefox http://localhost:3272/ &

for i in `seq 1 $NUM_OF_LOOPS`
  do
    displayState $i $NUM_OF_LOOPS
    mkdir ./out/$i #> /dev/null
    timeout 10m $JAVA_HOME -classpath ant-launcher-1.10.5.jar org.apache.tools.ant.launch.Launcher -e -f bin/jfsma20.xml run > ./out/$i/log.txt 2>&1
done

# Comment the following line if you do not want an analysis of the result
bash postprocess.sh

if [ -f "results.tar.gz" ]; 
  then
    rm results.tar.gz
fi

tar zcf results.tar.gz out/ 
      
