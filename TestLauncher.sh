#!/bin/bash

######################################################################
#
# Nicolas Cointe
#   date : 11 dec 2019
#   usage :
#   goal : just for debugging purpose
#
######################################################################

JAVA_HOME=$(which java)
ANT_HOME=$(which ant)

$JAVA_HOME -classpath ant-launcher-1.10.5.jar org.apache.tools.ant.launch.Launcher -e -f bin/jfsma20.xml run

