set encoding utf8
set terminal pdf size 10cm, 10cm
set output "average_estimated_goal1_probability.pdf"

#set title "Evolution of the goal 1 probability"
set xlabel "Step"
set ylabel "P(g1)"

set ticslevel 0

set xrange [0:30]
set yrange [0:1]

set format x "" # No x labels 

set size 1, 0.7
set origin 0, 0.3
set bmargin 0

set key top left
set multiplot

plot 'average.csv' using 1 with lines title 'ir1' lc rgb "#0099FF",\
	'average.csv' using 2 with lines title 'ir2' lc rgb "#FF0000",\
	'average.csv' using 3 with lines title 'ir3' lc rgb "#99FF00",\
	'average.csv' using 4 with lines title 'ir4' lc rgb "#00FF99"

unset title
set bmargin
set format x
set size 1.0, 0.3
set origin 0.0, 0.0
set tmargin 0
set autoscale y
#set format y "%1.0f"
#set ytics 25
#set ylabel "volume" offset 1
set ylabel "utilities"
plot 'average_utilities.csv' using 1 with lines title 'EUt' lc rgb "#00CC00",\
     'average_utilities.csv' using 2 with lines title 'EUo' lc rgb "#CC0000"
unset multiplot
