#!/bin/bash

######################################################################
#
# Nicolas Cointe
#   date : october 31st 2019
#   usage : postprocess.sh [NUMBER_OF_RUNS]
#   goal : analyse data from the social proof of concept
#
######################################################################

IC2S2_PAPER_FOLDER="../../papers/IC2S219/images"
OBFUSCATION_JOURNAL_PAPER_FOLDER="../../papers/BDI_obfuscation/images"                     
PROGRESS_BAR_LENGTH=20
LENGTH_LIMIT=18 # Minimum number of actions in a behavior to be selected for the chart

CSV_AVERAGE_RESULT=average.csv
CSV_AVERAGE_UTILITIES=average_utilities.csv

# HTML report generated
HTML_REPORT=report.html
if test -f "$HTML_REPORT"; then # remove the previous log file if any
   rm $HTML_REPORT
fi

# This function display an ascii progress bar
displayState()
{
  tput civis      -- invisible # Hide cursor
  echo -en " $1\t/$2\t$SECONDS s\t ["
  
  RATIO=$(echo "$1/$2" | bc -l)           # Evaluate the progress
  
  TIME_ESTIMATION=$(echo "$SECONDS/$RATIO" | bc -l )
  E_TIME_ESTIMATION=${TIME_ESTIMATION%.*} # Convert float to int

  NOW_H=$(date "+%-H")
  NOW_M=$(date "+%-M")
  NOW_S=$(date "+%-S")
  REMAIN_TIME=$(($E_TIME_ESTIMATION-$SECONDS)) 
 

  S_ESTIMATION=$(( ($REMAIN_TIME + $NOW_S) % 60 ))
  PROPAGATE=$(( ($REMAIN_TIME + $NOW_S) - $S_ESTIMATION ))
  M_ESTIMATION=$(( ($NOW_M + $PROPAGATE/60 ) %60 ))
  PROPAGATE=$(( ($NOW_M + $PROPAGATE/60 ) - $M_ESTIMATION))
  H_ESTIMATION=$(( ($NOW_H + $PROPAGATE/60 ) %24 ))

  for I in `seq 0 $PROGRESS_BAR_LENGTH`; do 

    CURSOR=$(echo "$I/$PROGRESS_BAR_LENGTH" | bc -l)
    LOWER=$(echo "$CURSOR<$RATIO" | bc -l) 
  
    if [ $LOWER -eq "1" ]; then
      echo -n "#"
    else
      echo -n "-"
    fi
  done

  echo -en "] Estimated end : $H_ESTIMATION h $M_ESTIMATION m $S_ESTIMATION s \r"
  tput cnorm   -- normal       # Restore the cursor visibility
}

if [[ $# != 0 ]]
  then
    NUM_OF_RUNS=$1
  else
    NUM_OF_RUNS=$(( `ls -l out | grep -c ^d` ))
		echo "$NUM_OF_RUNS runs detected"
fi

declare -a AGENTS=("ir1" "ir2" "ir3" "ir4")

# draw 
echo "<head>" >> $HTML_REPORT
echo "  <meta charset=\"utf-8\" />" >> $HTML_REPORT
echo "  <meta name=\"GENERATOR\" content=\"Keyword Counter\"/>" >> $HTML_REPORT
echo "  <title>Report of experiment</title>" >> $HTML_REPORT
echo "  <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">" >> $HTML_REPORT
echo "<script type=\"text/javascript\">function showSpoiler(obj){ var inner =obj.parentNode.getElementsByTagName(\"div\")[0]; if (inner.style.display == \"none\") inner.style.display = \"\";else inner.style.display = \"none\";}</script>" >> $HTML_REPORT
echo "</head>" >> $HTML_REPORT
echo "<body>" >> $HTML_REPORT

for i in `seq 1 $NUM_OF_RUNS`
	do
    cat out/$i/log.txt | grep -e "\[ir2\] I think" \
			| sed -e 's/\[ir2\] I think that for agent //g' > out/$i/inMind.csv # Get the probabilities
    cat out/$i/log.txt | grep -e "\[ir1\] EUT" \
      | sed -e 's/\[ir1\] EUT=\|EUO=\|Counter incremented :[0-9]*//g' > out/$i/utilities.csv # Get the utilities

		# Produce a file for each agent that contains 
		for ag in ${AGENTS[@]}; do
			# initial situation : equiprobability
			echo "0.33 0.33 0.33" > out/$i/$ag.csv 
   		cat out/$i/inMind.csv | grep -e "$ag" \
				| sed -e 's/ir[0-9]\|P([^)]*)=\|,\|[and ]//g' \
				| sed -e 's/0\./ 0./g' | sed -e's/-\|Agetppretlyperformectio[0-9]\|Coutericremete:[0-9]//g' >> out/$i/$ag.csv # TODO : solve the '-' problem
			sed -i '/^$/d' out/$i/$ag.csv # removes empty lines 
		done
	
		cp out/$i/ir*.csv .
		cp out/$i/utilities.csv .
		gnuplot plotAllAgentsG1_with_utilities.plot
    mv estimated_goal1_probability.pdf out/$i/.
		rm ir*.csv
		
    displayState $i $NUM_OF_RUNS
done

# Compute the averages
######################

echo ""
echo "Computes now the average probas"
NUM_OF_ACTIONS=$( wc -l out/1/ir1.csv | awk '{print $1}' )

if test -f "$CSV_AVERAGE_RESULT"; then # remove the previous file if any
   rm $CSV_AVERAGE_RESULT
fi

if test -f "$CSV_AVERAGE_UTILITIES"; then # remove the previous file if any
   rm $CSV_AVERAGE_UTILITIES
fi

echo "0 0" >> $CSV_AVERAGE_UTILITIES

re='^[+-]?[0-9]+([.][0-9]+)?$'

# For each step
for l in `seq 1 $NUM_OF_ACTIONS`
  do
		# For each Agent, compute the average
		for ag in ${AGENTS[@]}
			do
				
				THIS_AGENT_SCORE=0
				NUM_ERROR=0
				for i in `seq 1 $NUM_OF_RUNS`
  				do
						THIS_PROBA=$(sed "${l}q;d" out/$i/$ag.csv | awk '{print $1}' ) # got the proba of goal1 for this agent
						if [ $? -eq 0 ]; then
							if ! [[ $THIS_PROBA =~ $re ]] ; then # check the format of this value
	   						echo "error: '$THIS_PROBA' in out/$i/$ag.csv is not a number" >&2
								NUM_ERROR=$(echo "$NUM_ERROR+1" | bc)
								else
									THIS_AGENT_SCORE=$( echo "$THIS_AGENT_SCORE + $THIS_PROBA" | bc)
							fi
						else # TODO : solve 1 case that happen sometimes
							LINE=$(sed "${l}q;d" out/$i/$ag.csv)
  						echo "Problem with $LINE"
						fi				    
				done
				VALID_VALUES=$(echo "$NUM_OF_RUNS - $NUM_ERROR" | bc)
				THIS_AGENT_SCORE=$( echo "scale=4; $THIS_AGENT_SCORE / $VALID_VALUES" | bc)
				echo -n "$THIS_AGENT_SCORE " >> $CSV_AVERAGE_RESULT
		done

		# For each run, compute the average utilities
    AVG_EUO=0
    AVG_EUT=0
    NUM_O_ERR=0
    NUM_T_ERR=0
		for i in `seq 1 $NUM_OF_RUNS`
  		do
        THIS_EUO=$(sed "${l}q;d" out/$i/utilities.csv | awk '{print $1}' )
				if [ $? -eq 0 ]; then
					if ! [[ $THIS_EUO =~ $re ]] ; then # check the format of this value
	   				echo "error: '$THIS_EUO' in out/$i/utilities.csv is not a number" >&2
						NUM_O_ERR=$(echo "$NUM_O_ERR+1" | bc)
					else
						AVG_EUO=$( echo "$AVG_EUO + $THIS_EUO" | bc)
					fi
				else # TODO : solve 1 case that happen sometimes
					LINE=$(sed "${l}q;d" out/$i/utilities.csv)
  				echo "Problem with $LINE"
				fi				    
				
				THIS_EUT=$(sed "${l}q;d" out/$i/utilities.csv | awk '{print $2}' )
				if [ $? -eq 0 ]; then
					if ! [[ $THIS_EUT =~ $re ]] ; then # check the format of this value
	   				echo "error: '$THIS_EUT' in out/$i/utilities.csv is not a number" >&2
						NUM_O_ERR=$(echo "$NUM_T_ERR+1" | bc)
					else
						AVG_EUT=$( echo "$AVG_EUT + $THIS_EUT" | bc)
					fi
				else # TODO : solve 1 case that happen sometimes
					LINE=$(sed "${l}q;d" out/$i/utilities.csv)
  				echo "Problem with $LINE"
				fi
		done
		VALID_VALUES_O=$(echo "$NUM_OF_RUNS - $NUM_O_ERR" | bc)
		AVG_EUO=$( echo "scale=4; $AVG_EUO / $VALID_VALUES_O" | bc)

		VALID_VALUES_T=$(echo "$NUM_OF_RUNS - $NUM_T_ERR" | bc)
		AVG_EUT=$( echo "scale=4; $AVG_EUT / $VALID_VALUES_T" | bc)

		echo -n "$AVG_EUO " >> $CSV_AVERAGE_UTILITIES
    echo "$AVG_EUT" >> $CSV_AVERAGE_UTILITIES
		
    echo "" >> $CSV_AVERAGE_RESULT
		displayState $l $NUM_OF_ACTIONS
done
#gnuplot plotAverageG1.plot # plot the result
gnuplot plotAverageG1_with_utilities.plot # plot the result

echo ""
#echo "Final results generated"

# TODO : adapt automatically the file
gnuplot plotAllAgents.plot
gnuplot plotAllAgentsG1.plot

##### Now rendering the report #####
echo "Rendering HTML report"
echo -n "Generated on " >> $HTML_REPORT
date +%a >> $HTML_REPORT
date +%D >> $HTML_REPORT
echo -n " at " >> $HTML_REPORT
date +%T >> $HTML_REPORT
echo "<h1>General results</h1>" >> $HTML_REPORT


echo "<embed src=\"estimated_goals_probability.pdf\" width=\"500\" height=\"520\">" >> $HTML_REPORT
echo "<embed src=\"average_estimated_goal1_probability.pdf\" width=\"500\" height=\"520\">" >> $HTML_REPORT
echo "<p>Evolution of images (left chart) and evolution of the average images for P(g1) (right chart)</p>" >> $HTML_REPORT
echo "<br>" >> $HTML_REPORT

echo "    <table style=\"width:40%\">" >> $HTML_REPORT
echo "      <tr><td>Experiment</td><td>Pir1(G1)</td><td>Pir2(G1)</td><td>Pir3(G1)</td><td>Pir4(G1)</td></tr>" >> $HTML_REPORT

for i in `seq 1 $NUM_OF_RUNS`
  do
    echo -n "    <tr><td>" >> $HTML_REPORT
    echo -n "$i" >> $HTML_REPORT
    echo -n "</td>" >> $HTML_REPORT
    for ag in ${AGENTS[@]}; do
			echo -n "<td>" >> $HTML_REPORT
      PROBA=$(tail -n 1 out/$i/$ag.csv | awk '{print $1}' ) # | xargs printf "%.2f\n")
			echo -n "$PROBA" | xargs printf "%.2f\n" >> $HTML_REPORT
			echo -n "</td>" >> $HTML_REPORT
		done
    echo "</tr>" >> $HTML_REPORT # end of line
done
echo "    </table>" >> $HTML_REPORT
echo "<p>Final values of P(g1) for each agent in each run</p>" >> $HTML_REPORT

# Draw experiment by experiment
for i in `seq 1 $NUM_OF_RUNS`
	do
		echo "<h2>Experiment number $i</h2>" >> $HTML_REPORT
		echo "<div class=\"spoiler\"><br><input onclick=\"showSpoiler(this);\" value=\"Show/Hide\" type=\"button\"><div class=\"inner\" style=\"display: none;\">" >> $HTML_REPORT
		echo "<embed src=\"out/$i/estimated_goal1_probability.pdf\" width=\"500\" height=\"520\">" >> $HTML_REPORT
		echo "</div></div>" >> $HTML_REPORT
done

echo "</body>" >> $HTML_REPORT

echo -e "\nPostprocessing is done"


