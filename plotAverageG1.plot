set encoding utf8
set terminal pdf size 10cm, 10cm
set output "average_estimated_goal1_probability.pdf"

#set title "Evolution of the goal 1 probability"
set xlabel "Step"
set ylabel "P(g1)"

set ticslevel 0

set xrange [0:30]
set yrange [0:1]

set key top left

plot 'average.csv' using 1 with lines title 'ir1' lc rgb "#0099FF",\
	'average.csv' using 2 with lines title 'ir2' lc rgb "#FF0000",\
	'average.csv' using 3 with lines title 'ir3' lc rgb "#99FF00",\
	'average.csv' using 4 with lines title 'ir4' lc rgb "#00FF99"
