// Agent info_regulator in project aamas20_Social
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }

/* Knowledge bout available actions and GRF */
{ include("inc/common_knowledge.asl") }

/* Provides function to compute the entropy of a behavior */
{ include("inc/information_regulation.asl") }

/* To build coalitions */
{ include("inc/coalitions_building.asl") }

/* Provides plans activated on events */
{ include("inc/situation_awareness.asl") }

/* Trust building process */
{ include("inc/trust_building.asl") }

/* Costs-benefits evaluation */
{ include("inc/cost_benefits.asl") }

/* Costs-benefits evaluation */
{ include("inc/strategy_selection.asl") }

/* Generic functions */
{ include("inc/utils.asl") }

/* Initial beliefs and rules */
lastAction(none).

costRevealToOpponent(0.25).
costRevealToGreyAgents(0.125).
costRevealToNonCoButGood(0.01).
costRevealToTrustedAllies(0.0).

benefitInDreamTeam(0.25).
benefitOutDreamTeam(0.0).

myIdealCoalition([]).

/* Initial goals */

!start.

/* Plans */

+!start : true
	<-  .my_name(MyId);
		.wait(2000);
		lookupArtifact("marketPlace",MarketPlaceId);
		focus(MarketPlaceId);
		connect(MyId);
		+goalProba(MyId,0.33,0.33,0.33);
		!updateStrategy;
		!behave.
		
/* Decision Making process */

		
+!behave : feasible(A) 
		& myGoal(G) 
		& myStrategy(careless)
		& contributeTo(A,G)
		& .my_name(MyId)
		& goalProba(MyId,P1,P2,P3)
		& updateImageRatio(RATIO)
		& grf(A,DP1,DP2,DP3)
	<- .wait(2000);
		execute(MyId,A);
		-+lastAction(A);
		NewP1=P1+DP1*RATIO*(1.0-P1);
		NewP2=P2+DP2*RATIO*(1.0-P2);
		NewP3=1.0-NewP1-NewP2;
		.abolish(goalProba(MyId,_,_,_));
		+goalProba(MyId,NewP1,NewP2,NewP3);
		!behave.

// The agent performs an action if and only if the action
// is feasible, contributes to achieve the goal 
// and maximizes entropy
+!behave : feasible(A) 
		& myGoal(G) 
		& myStrategy(obfuscation)
		& contributeTo(A,G) 
		& .my_name(MyId)
		& goalProba(MyId,P1,P2,P3)
		& updateImageRatio(RATIO)
		& grf(A,DP1,DP2,DP3)
		& agentsList(AL)
		& expected_utility_transparency(EUT,AL)
		& expected_utility_obfuscation(EUO,AL)
		& not (
			action(B,_) 
			& feasible(B)
			& contributeTo(B,G) 
			& getEntropyOfHypotheticalB(MyId,A,HA)
			& getEntropyOfHypotheticalB(MyId,B,HB)
			& HB > HA
		)
	<- 	.wait(2000);
		execute(MyId,A);
		.println("EUT=",EUT," EUO=",EUO);
		-+lastAction(A);
		.abolish(goalProba(MyId,_,_,_));
		NewP1=P1+DP1*RATIO*(1.0-P1);
		NewP2=P2+DP2*RATIO*(1.0-P2);
		NewP3=1.0-NewP1-NewP2;
		+goalProba(MyId,NewP1,NewP2,NewP3);
		!behave.
		
// The agent performs an action if and only if the action
// is feasible, contributes to achieve the goal 
// and minimizes entropy
+!behave : feasible(A) 
		& myGoal(G) 
		& myStrategy(transparency)
		& contributeTo(A,G) 
		& .my_name(MyId)
		& goalProba(MyId,P1,P2,P3)
		& updateImageRatio(RATIO)
		& grf(A,DP1,DP2,DP3)
		& agentsList(AL)
		& expected_utility_transparency(EUT,AL)
		& expected_utility_obfuscation(EUO,AL)
		& not (
			action(B,_) 
			& feasible(B)
			& contributeTo(B,G) 
			& getEntropyOfHypotheticalB(MyId,A,HA)
			& getEntropyOfHypotheticalB(MyId,B,HB)
			& HB < HA
		)
	<- 	.wait(2000);
		execute(MyId,A);
		.println("EUT=",EUT," EUO=",EUO);
		-+lastAction(A);
		.abolish(goalProba(MyId,_,_,_));
		NewP1=P1+DP1*RATIO*(1.0-P1);
		NewP2=P2+DP2*RATIO*(1.0-P2);
		NewP3=1.0-NewP1-NewP2;
		+goalProba(MyId,NewP1,NewP2,NewP3);
		!behave.
		
+!behave : agentsList(AL)
		& expected_utility_transparency(EUT,AL)
		& expected_utility_obfuscation(EUO,AL)
		<- .println("There is no problem with expected utilities as EUT=", EUT, " and EUO=",EUO).		
		

		
+!behave : agentsList(AL)
		& expected_utility_transparency(EUT,AL)
		<- .println("There is maybe a problem with EUO but EUT=", EUT).
		

		
+!behave : agentsList(AL)
		& expected_utility_obfuscation(EUO,AL)
		<- .println("There is maybe a problem with EUT but EUO=",EUO).
		
// There is no more action to perform
+!behave : true
	<- println("Goal achieved").

// uncomment the include below to have an agent compliant with its organisation
//{ include("$moiseJar/asl/org-obedient.asl") }
