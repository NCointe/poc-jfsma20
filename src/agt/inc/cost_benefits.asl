
/* FUNCTIONS TO COMPUTE COSTS */

// If the agent is identified as an opponent
costRevealGoalTo(Cost,Agent):-
  trust(Agent,AgentGoal)
  & costRevealToOpponent(Cost)
  & not myGoal(AgentGoal).
  
// If the agent is in my grey zone
costRevealGoalTo(Cost,Agent):-
  not trust(Agent,_)
  & costRevealToGreyAgents(Cost).
  
// If the agent is trusted but not in my ideal coalition
costRevealGoalTo(Cost,Agent):-
  trust(Agent,AgentGoal)
  & costRevealToNonCoButGood(Cost)
  & myGoal(AgentGoal)
  & not inDreamTeam(Agent).
  
// If the agent is in my ideal coalition
costRevealGoalTo(Cost,Agent):-
  trust(Agent,AgentGoal)
  & myGoal(AgentGoal)
  & inDreamTeam(Agent)
  & costRevealToTrustedAllies(Cost).
  
// If I trust this agent to achieve my goal
benefitRevealGoalTo(Benefit,Agent):-
  benefitInDreamTeam(Benefit)
  & trust(Agent,AgentGoal)
  & myGoal(AgentGoal).
  
// If I trust this agent to achieve another goal than mine
benefitRevealGoalTo(Benefit,Agent):-
  benefitOutDreamTeam(Benefit)
  & trust(Agent,AgentGoal)
  & not myGoal(AgentGoal).
 
// If do not trust this agent
benefitRevealGoalTo(Benefit,Agent):-
  benefitOutDreamTeam(Benefit)
  & not trust(Agent,AgentGoal)
  & myGoal(AgentGoal).

totalCost(0,[]).
totalCost(Total,[HeadAgent|Tail]):-
	costRevealGoalTo(CostH,HeadAgent)
	& totalCost(CostT,Tail)
	& Total = CostH+CostT.

totalBenefit(0,[]).	
totalBenefit(Total,[HeadAgent|Tail]):-
	benefitRevealGoalTo(CostH,HeadAgent)
	& totalBenefit(CostT,Tail)
	& Total = CostH+CostT. 

// NEW MODEL

probCT(1.0):- behaviorImpactsSocial. // If the agent believe in the impact of IRS on its social relations
probCT(0.0):- not behaviorImpactsSocial. // otherwise
probCO(0.0).

probCO(PCO):- probCT(PCT) & PCO=1.0-PCT.

expected_utility_transparency(EUT,AgentList):- probCT(P)
	& totalBenefit(B, AgentList)
	& totalCost(C,AgentList)
	& EUT = (P*B)-C.
	
expected_utility_obfuscation(EUO,AgentList):- probCO(P)
	& totalBenefit(B, AgentList)
	& EUO = P*B. // TODO : Should include "-CO" which is the total cost associated with an obfuscating behavior (here 0)

  
  