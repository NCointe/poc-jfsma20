getEntropyOfHypotheticalB(Agent,Action,H):-
	goalProba(Agent,PG1,PG2,PG3)
	& grf(Action,DPG1,DPG2,DPG3)
	& jia.shannonEntropy(H,PG1+DPG1,PG2+DPG2,PG3+DPG3).