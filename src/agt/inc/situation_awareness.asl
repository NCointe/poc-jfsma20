

/********** Old implementation **********
// Agent already perceived before
+action_done(Agent,ActionString): 
	goalProba(Agent,P1,P2,P3)
	& action(Action,ActionString)
	& grf(Action,DP1,DP2,DP3)
	<- 	.abolish(goalProba(Agent,_,_,_));
		+goalProba(Agent,P1+DP1,P2+DP2,P3+DP3);
		println("I think that for agent ",Agent," P(g1)=",P1+DP1,", P(g2)=",P2+DP2," and P(g3)=",P3+DP3);
		!checkTrust(Agent);
		!updateStrategy.
	
// Agent met for the first time
+action_done(Agent,ActionString):
	action(Action,ActionString)
	& grf(Action,DP1,DP2,DP3)
	<- +goalProba(Agent,0.33+DP1,0.33+DP2,0.33+DP3);
		println("I think that for agent ",Agent," P(g1)=",0.33+DP1,", P(g2)=",0.33+DP2," and P(g3)=",0.33+DP3);
		!updateCoalition(Agent);
		!updateStrategy.
*/		

/********** New implementation **********/
// Agent already perceived before
@updateImage[atomic]
+action_done(Agent,ActionString): 
	goalProba(Agent,P1,P2,P3)
	& updateImageRatio(RATIO)
	& action(Action,ActionString)
	& grf(Action,DP1,DP2,DP3)
	<- 	NewP1=P1+DP1*RATIO*(1.0-P1);
		NewP2=P2+DP2*RATIO*(1.0-P2);
		NewP3=1.0-NewP1-NewP2;
		.abolish(goalProba(Agent,_,_,_));
		+goalProba(Agent,NewP1,NewP2,NewP3);
		println("I think that for agent ",Agent," P(g1)=",NewP1,", P(g2)=",NewP2," and P(g3)=",NewP3);
		!checkTrust(Agent);
		!updateStrategy.
	
// Agent met for the first time
@createImage[atomic]
+action_done(Agent,ActionString):
	action(Action,ActionString)
	& updateImageRatio(RATIO)
	& grf(Action,DP1,DP2,DP3)
	<- 	NewP1=0.33+DP1*RATIO*0.67;
		NewP2=0.33+DP2*RATIO*0.67;
		NewP3=1.0-NewP1-NewP2;
		+goalProba(Agent,NewP1,NewP2,NewP3);
		println("I think that for agent ",Agent," P(g1)=",NewP1,", P(g2)=",NewP2," and P(g3)=",NewP3);
		!checkTrust(Agent);
		!updateStrategy.

//+action_done(Agent,Action)<- println("oupdate for ",Agent,Action,"!").

