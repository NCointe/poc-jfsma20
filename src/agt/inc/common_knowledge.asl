// Agent common_knowledge in project aamas20_Social
feasible(A):- action(A,_) 
		& not lastAction(A)
		& .my_name(MyId)
		& goalProba(MyId,P1,P2,P3)
		& updateImageRatio(RATIO)
		& grf(A,DP1,DP2,DP3)
		& S1=P1+DP1*RATIO*(1.0-P1)
		& S1<=1
		& S1>=0
		& S2=P2+DP2*RATIO*(1.0-P2)
		& S2 >=0
		& S2 <=1
		& S3=1.0-S1-S2
		& S3 >=0
		& S3 <=1.

delayBetweenActions(1000).

updateImageRatio(1.5).

action(a0,"a0").
action(a1,"a1").
action(a2,"a2").
action(a3,"a3").
action(a4,"a4").
action(a5,"a5").
action(a6,"a6").
action(a7,"a7").
action(a8,"a8").
action(a9,"a9").
action(a10,"a10").
action(a11,"a11").
action(a12,"a12").

contributeTo(a0,g1).
contributeTo(a0,g2).
contributeTo(a0,g3).

contributeTo(a1,g1).

contributeTo(a2,g2).

contributeTo(a3,g1).
contributeTo(a3,g2).

contributeTo(a4,g3).

contributeTo(a5,g1).
contributeTo(a5,g3).

contributeTo(a6,g2).
contributeTo(a6,g3).

contributeTo(a7,g2).
contributeTo(a7,g3).

contributeTo(a8,g1).
contributeTo(a8,g3).

contributeTo(a9,g1).
contributeTo(a9,g2).

contributeTo(a10,g1).
contributeTo(a10,g3).

contributeTo(a11,g1).
contributeTo(a11,g2).

contributeTo(a12,g2).
contributeTo(a12,g3).

grf(a0,0.0,0.0,0.0).
grf(a1,0.05,-0.025,-0.025).
grf(a2,-0.025,0.05,-0.025).
grf(a3,0.025,0.025,-0.05).
grf(a4,-0.025,-0.025,0.05).
grf(a5,0.025,-0.05,0.025).
grf(a6,-0.05,0.025,0.025). 
grf(a7,-0.1,0.1,0.0). // 0g3
grf(a8,0.0,-0.1,0.1). //Og1
grf(a9,0.1,0.0,-0.1). //Og2
grf(a10,0.1,-0.1,0.0). // Og3
grf(a11,0.0,0.1,-0.1). // Og1
grf(a12,-0.1,0.0,0.1). // Og2


