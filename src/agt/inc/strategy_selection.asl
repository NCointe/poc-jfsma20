

	
	
/*** Evaluation of the most interesting strategy ***/

// Switch to careless
@switchCareless[atomic]
+!updateStrategy: not behaviorImpactsSocial & not myStrategy(careless)
	<- 	.abolish(myStrategy(_)); // forget the previous strategy
		+myStrategy(careless);   // adopt a careless strategy
		println("--------------------- I switch to a careless strategy ---------------------").
		
// OLD MODEL
// Switch to obfuscator
/* 
@switchObfuscator[atomic]
+!updateStrategy: behaviorImpactsSocial 
				& not myStrategy(obfuscation) 
				& agentsList(L)
				& totalCost(TC,L)
				& totalBenefit(TB,L)
				& TC>TB
	<- 	.abolish(myStrategy(_)); // forget the previous strategy
		+myStrategy(obfuscation);   // adopt a careless strategy
		println("--------------------- I switch to an obfuscation strategy ---------------------").
*/		
// Switch to transparency
/* 
@switchtransparent[atomic]
+!updateStrategy: behaviorImpactsSocial 
				& not myStrategy(transparency) 
				& agentsList(L)
				& totalCost(TC,L)
				& totalBenefit(TB,L)
				& not TC>TB
	<- 	.abolish(myStrategy(_)); // forget the previous strategy
		+myStrategy(transparency);   // adopt a careless strategy
		println("--------------------- I switch to a transparent strategy ---------------------").
*/

// NEW MODEL
// Switch to obfuscator
@switchObfuscator[atomic]
+!updateStrategy: behaviorImpactsSocial 
				& not myStrategy(obfuscation) 
				& agentsList(L)
				& expected_utility_transparency(EUT,L)
				& expected_utility_obfuscation(EUO,L)
				& EUO>EUT
	<- 	.abolish(myStrategy(_)); // forget the previous strategy
		+myStrategy(obfuscation);   // adopt a careless strategy
		println("--------------------- I switch to an obfuscation strategy ---------------------").

// Switch to transparency
@switchtransparent[atomic]
+!updateStrategy: behaviorImpactsSocial 
				& not myStrategy(transparency) 
				& agentsList(L)
				& expected_utility_transparency(EUT,L)
				& expected_utility_obfuscation(EUO,L)
				& EUO<EUT
	<- 	.abolish(myStrategy(_)); // forget the previous strategy
		+myStrategy(transparency);   // adopt a careless strategy
		println("--------------------- I switch to a transparent strategy ---------------------").
		
+!updateStrategy: true.
//      <-  println("--------------------- Nothing to be done --------------------"). // Otherwise, nothing to be changed



