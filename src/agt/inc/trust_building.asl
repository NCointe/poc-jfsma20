
trustThreshold(0.65).



// TODO : make this more generic
+!checkTrust(Ag): not trust(Ag,g1) 
			& goalProba(Ag,P1,_,_)
			& trustThreshold(T)
			& P1>T
			<- +trust(Ag,g1);
				println("I trust ", Ag, " for trying to achieve g1").
			
+!checkTrust(Ag): trust(Ag,g1) 
			& goalProba(Ag,P1,_,_)
			& trustThreshold(T)
			& P1<T
			<- -trust(Ag,g1);
				println("I do not trust anymore ", Ag, " for trying to achieve g1").

+!checkTrust(Ag): not trust(Ag,g2) 
			& goalProba(Ag,_,P2,_)
			& trustThreshold(T)
			& P2>T
			<- +trust(Ag,g2);
				println("I trust ", Ag, " for trying to achieve g2").
			
+!checkTrust(Ag): trust(Ag,g2) 
			& goalProba(Ag,_,P2,_)
			& trustThreshold(T)
			& P2<T
			<- -trust(Ag,g2);
				println("I do not trust anymore ", Ag, " for trying to achieve g2").

+!checkTrust(Ag): not trust(Ag,g3) 
			& goalProba(Ag,_,_,P3)
			& trustThreshold(T)
			& P3>T
			<- +trust(Ag,g3);
				println("I trust ", Ag, " for trying to achieve g3").
			
+!checkTrust(Ag): trust(Ag,g3) 
			& goalProba(Ag,_,_,P3)
			& trustThreshold(T)
			& P3<T
			<- -trust(Ag,g3);
				println("I do not trust anymore ", Ag, " for trying to achieve g3").
				
+!checkTrust(_) : true. // If nothing to be done
				
				
				