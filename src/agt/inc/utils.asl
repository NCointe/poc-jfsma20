

isInList(Element,[Element]).
isInList(Element,[Element|_]).
isInList(Element,[_|Tail]):-isInList(Element,Tail).

addInList(Element,[],[Element]).
addInList(Element,[Head|Tail],[Head|NewTail]):-addInList(Element,Tail,NewTail). 