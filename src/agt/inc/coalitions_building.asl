

// Made atomic as the agentList is removed and added again
//   Called from action_done in situation_awareness.asl
@updatelist[atomic]
+!updateCoalition(Agent): agentsList(L)
		& not isInList(Agent,L)
		& addInList(Agent,L,NL)
		& totalCost(Cost,NL)
	<- .abolish(agentsList(_));
	   +agentsList(NL);
	   .abolish(revealCost(_));
	   +revealCost(Cost).
			
+!updateCoalition(Agent): 
		trust(Agent,Goal)
		& myGoal(Goal)
		& myIdealCoalition(L)
		& not isInList(Agent,L)
	<- println("Woops").