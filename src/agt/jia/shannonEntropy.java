// Internal action code for project abstractObfuscation

package jia;

import jason.asSemantics.*;
import jason.asSyntax.*;

/**
 * This internal action unifies the first argument with the Shannon entropy of all the next arguments
 *
 */
 
public class shannonEntropy extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        double sum=0.0;
        for(int i=1; i<args.length; i++) // computes the sum of the actions done
        {
        	NumberTerm n = (NumberTerm) args[i];
        	sum+=n.solve();
        }
        
        double entropy =0.0;
        for(int i=1; sum> 0.0 && i<args.length; i++) // computes the entropy
        {
        	NumberTerm n = (NumberTerm) args[i];
        	if (n.solve()>0)
        		entropy -= n.solve()/sum * Math.log(n.solve()/sum)/Math.log(2);
        }

        NumberTerm result = new NumberTermImpl(entropy);
        return un.unifies(result, args[0]);
    }
}
