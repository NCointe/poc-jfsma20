// CArtAgO artifact code for project aamas20_Social

package aamas20Social;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import cartago.*;

public class MarketPlace extends Artifact {
	
	protected double pfail=0.05;
	protected long maxActionsPerAgents=31;
	
	protected String agentTargeted="ir1";
	protected long countedActions=0;
	
	protected LinkedList<String> agentNames = new LinkedList<String>();
	
	protected ArrayList<String> actionsList = new ArrayList<String>();

	protected File dataFile= null;
		
	boolean loaded = false;
	boolean loadingEnded = false;
    	
	@OPERATION
    void init()
	{
		System.out.println("Init");
		actionsList.add("a1");
		actionsList.add("a2");
		actionsList.add("a3");
		actionsList.add("a4");
		actionsList.add("a5");
		actionsList.add("a6");
		actionsList.add("a7");
		actionsList.add("a8");
		actionsList.add("a9");
		actionsList.add("a10");
		actionsList.add("a11");
		actionsList.add("a12");
    }
	
	@OPERATION
    void connect(String agentName) 
	{
		if(!loaded)
		{
			loaded=true;
		}
		if(! agentNames.contains(agentName))
		{
			agentNames.push(agentName);
			System.out.println(agentName + " connected");
		}
	}
	
	@OPERATION
	void execute(String agentName, String actionName)
	{
		double rand = Math.random();
		if(agentTargeted==null)
		{
			agentTargeted=agentName;
			System.out.println("Agent set :" + agentName);
		}
		if(agentTargeted.contentEquals(agentName))
		{
			countedActions++;
			System.out.println("Counter incremented :" + countedActions);
		}
		if(countedActions==maxActionsPerAgents)
		{
			System.out.println("Maximum number of actions reached.");
			System.exit(0);
		}
		
		/* We use here a random test */ 
		if(rand>pfail )
		{
			System.out.println("Agent " + agentName + " apparently performed action " + actionName);
			signal("action_done",agentName,actionName);
		}
		else
		{
			ArrayList<String> toPickIn=actionsList;
			
			// Removes the right action
			for(int i =0; i<toPickIn.size(); i++)
				if(toPickIn.get(i)==actionName)
					toPickIn.remove(i);
			
			String action = toPickIn.get((int) (Math.random()*(double)(toPickIn.size())));
			System.out.println("Agent " + agentName + " apparently performed action " + action);
			signal("action_done",agentName,action);
		}	
	}
}

